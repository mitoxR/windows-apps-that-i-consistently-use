# 剪切板管理工具

## CopyQ

![CopyQ-icon](http://hluk.github.io/CopyQ/images/logo.png)

Official [GitHub](http://hluk.github.io/CopyQ/) | [Sourceforge](https://sourceforge.net/projects/copyq/)

Download [Release](https://github.com/hluk/CopyQ/releases) | [Sourceforge](https://sourceforge.net/projects/copyq/files/)

CopyQ 是一款功能强大的剪切板管理器，支持富文本，图片，文件路径保存和复制，编辑复制内容，脚本方式管理，分tab管理，拖拽粘贴等，除此之外的基本的查删改排都是支持的。
使用C++编写，文件小巧，功能强大而不会占用太多的系统资源。

## ClipboardX

